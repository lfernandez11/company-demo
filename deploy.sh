case "$1" in
  (dev|qa|prod)
    echo "Iniciando despliegue"
    git pull

    CID=$(docker ps -aqf "name=company-demo-server")
    echo $CID

    echo "Detendiendo contenedor"
    docker stop $CID

    echo "Borrando contenedor"
    docker rm $CID

    echo "Borrando imagen"
    docker rmi company-demo-server:v1

    echo "Creando jar de spring"
    gradle build -x test

    docker build -t company-demo-server:v1 .
    docker run -p 8505:8505 -e SPRING_PROFILES_ACTIVE=$1  --network top-net --name company-demo-server -d company-demo-server:v1

    echo "Terminando despliegue"
  ;;
  (*) echo "Unknown environment. Possible values can be dev, qa, prod." ;;
esac
