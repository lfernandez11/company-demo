UPDATE companies
SET user_id = CASE WHEN :USER_ID::varchar IS NULL THEN user_id ELSE :USER_ID END,
    name = CASE WHEN :NAME::varchar IS NULL THEN name ELSE :NAME END
WHERE company_id = :COMPANY_ID
RETURNING *