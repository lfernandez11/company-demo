package com.top.wallet.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

    private SecurityUtil() {
    }

    public static Integer getUserId() {
        Authentication aut = SecurityContextHolder.getContext().getAuthentication();
        return Integer.parseInt(aut.getPrincipal().toString());
    }

}
