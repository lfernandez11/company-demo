package com.top.wallet.domain.filters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InListFilter implements Filter {

    String key;
    List<String> values;

    @Override
    public Filter build(String key, List<String> values) {
        List<String> valuesWithoutEmpty = values.stream()
                .filter(value -> !StringUtils.isEmpty(value))
                .collect(Collectors.toList());

        return new InListFilterBuilder()
                .key(key)
                .values(valuesWithoutEmpty)
                .build();
    }
}
