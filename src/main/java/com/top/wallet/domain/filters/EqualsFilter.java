package com.top.wallet.domain.filters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EqualsFilter implements Filter {

    String key;
    String value;

    @Override
    public Filter build(String key, List<String> values) {
        return new EqualsFilterBuilder()
                .key(key)
                .value(values.get(0))
                .build();
    }
}
