package com.top.wallet.adapter.rest.model;

import com.top.wallet.domain.Contractor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateContractorRequestModel {

    private String userId;
    private String name;

    public static CreateContractorRequestModel fromDomain(Contractor contractor) {
        return CreateContractorRequestModel.builder()
                .userId(contractor.getUserId())
                .name(contractor.getName())
                .build();
    }
}
