package com.top.wallet.adapter.controller;

import com.top.wallet.adapter.controller.model.PageResponse;
import com.top.wallet.config.ErrorCode;
import com.top.wallet.domain.Page;
import com.top.wallet.adapter.controller.model.CompanyRestModel;
import com.top.wallet.adapter.controller.model.CreateCompanyRequestBody;
import com.top.wallet.adapter.controller.model.PatchCompanyRequestBody;
import com.top.wallet.adapter.controller.model.UpdateCompanyRequestBody;
import com.top.wallet.application.port.in.*;
import com.top.wallet.domain.Company;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/api/companies")
public class CompanyController {

    private final GetCompanyQuery getCompanyQuery;
    private final CreateCompanyCommand createCompanyCommand;
    private final GetCompaniesListQuery getCompaniesListQuery;
    private final GetCompaniesPageQuery getCompaniesPageQuery;
    private final UpdateCompanyCommand updateCompanyCommand;
    private final PatchCompanyCommand patchCompanyCommand;
    private final DeleteCompanyCommand deleteCompanyCommand;

    private static final Set<String> nonFilterKeys = Set.of("page", "size");

    public CompanyController(GetCompanyQuery getCompanyQuery,
                             CreateCompanyCommand createCompanyCommand,
                             GetCompaniesListQuery getCompaniesListQuery,
                             GetCompaniesPageQuery getCompaniesPageQuery,
                             UpdateCompanyCommand updateCompanyCommand,
                             PatchCompanyCommand patchCompanyCommand,
                             DeleteCompanyCommand deleteCompanyCommand) {
        this.getCompanyQuery = getCompanyQuery;
        this.createCompanyCommand = createCompanyCommand;
        this.getCompaniesListQuery = getCompaniesListQuery;
        this.getCompaniesPageQuery = getCompaniesPageQuery;
        this.updateCompanyCommand = updateCompanyCommand;
        this.patchCompanyCommand = patchCompanyCommand;
        this.deleteCompanyCommand = deleteCompanyCommand;
    }


    @GetMapping("/{company_id}")
    @ApiOperation("Get Company by Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Company found"),
            @ApiResponse(code = 400, message = "Invalid id"),
            @ApiResponse(code = 401, message = "Invalid token"),
            @ApiResponse(code = 403, message = "Permission denied"),
            @ApiResponse(code = 404, message = "Company not found"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 503, message = "Service unavailable"),})
    public CompanyRestModel getCompany(@PathVariable("company_id") Integer companyId) {
        GetCompanyQuery.Data data = GetCompanyQuery.Data.builder()
                .companyId(companyId)
                .build();
        CompanyRestModel companyResult = CompanyRestModel.fromDomain(getCompanyQuery.execute(data));
        return companyResult;
    }


    @PostMapping
    @ApiOperation("Create Company")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Company created"),
            @ApiResponse(code = 400, message = "Malformed request syntax"),
            @ApiResponse(code = 401, message = "Invalid token"),
            @ApiResponse(code = 403, message = "Permission denied"),
            @ApiResponse(code = 409, message = "Company already exists"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 503, message = "Service unavailable"),})
    public ResponseEntity<CompanyRestModel> createCompany(@RequestBody @Valid CreateCompanyRequestBody body) {
        CreateCompanyCommand.Data data = CreateCompanyCommand.Data.builder()
                .userId(body.getUserId())
                .name(body.getName())
                .build();
        CompanyRestModel companyResult = CompanyRestModel.fromDomain(createCompanyCommand.execute(data));
        return ResponseEntity.status(HttpStatus.CREATED).body(companyResult);
    }


    @GetMapping
    @ApiOperation("Get Companies by filters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request processed correctly"),
            @ApiResponse(code = 400, message = "Malformed request syntax"),
            @ApiResponse(code = 401, message = "Invalid token"),
            @ApiResponse(code = 403, message = "Permission denied"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 503, message = "Service unavailable"),})
    public ResponseEntity getCompanies(
            @RequestParam(name = "page", required = false) Integer page,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam Map<String, String> queryParams
    ) {
        ResponseEntity response;

        if (queryParams.containsKey("page") && queryParams.containsKey("size")) {
            Map<String, String> filtersMap = new HashMap<>(queryParams);
            filtersMap.keySet().removeAll(nonFilterKeys);
            response = getCompaniesPage(page, size, filtersMap);
        } else {
            response = getCompaniesList(queryParams);
        }

        return response;
    }

    private ResponseEntity getCompaniesList(Map<String, String> filtersMap) {
        GetCompaniesListQuery.Data data = GetCompaniesListQuery.Data.builder()
                .filtersMap(filtersMap)
                .build();
        List<Company> companies = getCompaniesListQuery.execute(data);
        return ResponseEntity.ok(CompanyRestModel.fromListDomain(companies));
    }

    private ResponseEntity getCompaniesPage(Integer page, Integer size, Map<String, String> filtersMap) {
        GetCompaniesPageQuery.Data data = GetCompaniesPageQuery.Data.builder()
                .page(page)
                .size(size)
                .filtersMap(filtersMap)
                .build();
        Page<Company> companies = getCompaniesPageQuery.execute(data);
        PageResponse<CompanyRestModel> companiesPageResponse = new PageResponse<CompanyRestModel>()
                .fromDomain(companies, CompanyRestModel::fromDomain);
        return ResponseEntity.ok(companiesPageResponse);
    }


    @PutMapping("/{company_id}")
    @ApiOperation("Update Company")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Company updated"),
            @ApiResponse(code = 400, message = "Malformed request syntax"),
            @ApiResponse(code = 401, message = "Invalid token"),
            @ApiResponse(code = 403, message = "Permission denied"),
            @ApiResponse(code = 409, message = "Company already exists"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 503, message = "Service unavailable"),})
    public CompanyRestModel updateCompany(@PathVariable("company_id") Integer companyId, @RequestBody @Valid UpdateCompanyRequestBody body) {
        UpdateCompanyCommand.Data data = UpdateCompanyCommand.Data.builder()
                .companyId(companyId)
                .userId(body.getUserId())
                .name(body.getName())
                .build();
        CompanyRestModel companyResult = CompanyRestModel.fromDomain(updateCompanyCommand.execute(data));
        return companyResult;
    }


    @PatchMapping("/{company_id}")
    @ApiOperation("Patch Company with non null values")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Company patched"),
            @ApiResponse(code = 400, message = "Malformed request syntax"),
            @ApiResponse(code = 401, message = "Invalid token"),
            @ApiResponse(code = 403, message = "Permission denied"),
            @ApiResponse(code = 409, message = "Company already exists"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 503, message = "Service unavailable"),})
    public CompanyRestModel patchCompany(@PathVariable("company_id") Integer companyId, @RequestBody @Valid PatchCompanyRequestBody body) {
        PatchCompanyCommand.Data data = PatchCompanyCommand.Data.builder()
                .companyId(companyId)
                .userId(body.getUserId())
                .name(body.getName())
                .build();
        CompanyRestModel companyResult = CompanyRestModel.fromDomain(patchCompanyCommand.execute(data));
        return companyResult;
    }


    @DeleteMapping("/{company_id}")
    @ApiOperation("Delete Company")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Company deleted"),
            @ApiResponse(code = 401, message = "Invalid token"),
            @ApiResponse(code = 403, message = "Permission denied"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 503, message = "Service unavailable"),
    })
    public ResponseEntity<Void> deleteCompany(@PathVariable("company_id") Integer companyId) {
        DeleteCompanyCommand.Data data = DeleteCompanyCommand.Data.builder()
                .companyId(companyId)
                .build();
        deleteCompanyCommand.execute(data);
        return ResponseEntity.noContent().build();
    }
}
