package com.top.wallet.adapter.controller.model;

import com.top.wallet.domain.Company;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@AllArgsConstructor
@NoArgsConstructor
public class CompanyRestModel {

    private Integer companyId;
    private String userId;
    private String name;

    public static CompanyRestModel fromDomain(Company company) {
        return CompanyRestModel.builder()
            .companyId(company.getCompanyId())
            .userId(company.getUserId())
            .name(company.getName())
            .build();
    }

    public static List<CompanyRestModel> fromListDomain(List<Company> companies) {
        return companies.stream()
            .map(CompanyRestModel::fromDomain)
            .collect(Collectors.toList());
    }
}
