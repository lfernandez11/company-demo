package com.top.wallet.adapter.jdbc.model;

import com.top.wallet.domain.Company;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyJdbcModel {

    private Integer companyId;
    private String userId;
    private String name;

    public Company toDomain() {
        return Company.builder()
            .companyId(companyId)
            .userId(userId)
            .name(name)
            .build();
    }
}
