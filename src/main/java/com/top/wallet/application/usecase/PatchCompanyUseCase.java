package com.top.wallet.application.usecase;

import com.top.wallet.application.port.in.PatchCompanyCommand;
import com.top.wallet.application.port.out.CompanyRepository;
import com.top.wallet.domain.Company;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;

@Component
@Slf4j
public class PatchCompanyUseCase implements PatchCompanyCommand {

    private final CompanyRepository companyRepository;

    public PatchCompanyUseCase(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    @Override
    public Company execute(Data data) {
        return companyRepository.patch(data.toDomain());
    }
}