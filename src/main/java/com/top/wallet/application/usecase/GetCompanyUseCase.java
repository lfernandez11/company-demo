package com.top.wallet.application.usecase;

import com.top.wallet.application.port.in.GetCompanyQuery;
import com.top.wallet.application.port.out.CompanyRepository;
import com.top.wallet.domain.Company;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;

@Slf4j
@Component
public class GetCompanyUseCase implements GetCompanyQuery {

    private final CompanyRepository companyRepository;

    public GetCompanyUseCase(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    @Override
    public Company execute(Data data) {
        return companyRepository.findById(data.getCompanyId());
    }
}
