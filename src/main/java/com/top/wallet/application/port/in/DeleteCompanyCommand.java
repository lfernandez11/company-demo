package com.top.wallet.application.port.in;

import lombok.Builder;
import lombok.Value;

public interface DeleteCompanyCommand {

    void execute(Data data);

    @Value
    @Builder
    class Data {
        Integer companyId;
    }
}
