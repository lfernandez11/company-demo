package com.top.wallet.application.exception;

import com.top.wallet.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityConflictException extends HttpErrorException {

    public EntityConflictException(ErrorCode errorCode) {

        super(HttpStatus.CONFLICT.value(), errorCode);
    }
}
