package com.top.wallet.application.usecase


import com.top.wallet.application.exception.RepositoryNotAvailableException
import com.top.wallet.domain.filters.EqualsFilter
import com.top.wallet.domain.filters.Filter
import com.top.wallet.application.port.in.GetCompaniesListQuery
import com.top.wallet.application.port.out.CompanyRepository
import com.top.wallet.domain.Company
import spock.lang.Specification

class GetCompaniesListUseCaseTest extends Specification {

    private static final int COMPANY_ID = 1121
    private static final String USER_ID = "16a5dd32-3d2a-4aa5-9980-6b3e2de305a0"
    private static final String NAME = "Magios"

    CompanyRepository companyRepository = Mock(CompanyRepository)

    GetCompaniesListUseCase target = new GetCompaniesListUseCase(companyRepository)

    def "given the filters {queryParams} when GetCompaniesListUseCase is executed then repository is called correctly and response is as expected"() {
        given:
        def data = new GetCompaniesListQuery.Data(queryParams)
        def aCompany = new Company(COMPANY_ID, USER_ID, NAME)
        def otherCompany = new Company(COMPANY_ID, USER_ID, NAME)
        def expected = [aCompany, otherCompany]

        when:
        def response = target.execute(data)

        then:
        1 * companyRepository.findAll(filters) >> expected
        response == expected

        where:
        queryParams              | filters
        Collections.emptyMap()   | Collections.emptyList()
        [name: "equals," + NAME] | [new EqualsFilter("name", NAME)]
    }

    def "given any filters when GetCompaniesListUseCase is executed and the repository throws {exception} then {expected} is thrown"() {
        given:
        def data = new GetCompaniesListQuery.Data(Collections.emptyMap())
        companyRepository.findAll(_ as List<Filter>) >> { throw exception }

        when:
        target.execute(data)

        then:
        thrown(expected)

        where:
        expected                        | exception
        RepositoryNotAvailableException | new RepositoryNotAvailableException()
        Exception                       | new Exception()
    }
}
